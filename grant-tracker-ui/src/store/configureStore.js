import { createStore, applyMiddleware } from 'redux';
import thunkMiddleWare from 'redux-thunk';
import rootReducer from '../reducers';


export default function configureStore(initialState) {
    // composeEnhancers()
    return createStore(rootReducer, initialState, applyMiddleware(thunkMiddleWare));
}

