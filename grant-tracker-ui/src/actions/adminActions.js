import * as types from '../types/appTypes';
import api from '../api';

function getAllowedValuesSuccess({ allowedValues }) {
    return {
        type: types.GET_ALLOWED_VALUES_SUCCESS,
        allowedValues
    };
}

function getAllowedValuesError({ error }) {
    return {
        type: types.GET_ALLOWED_VALUES_ERROR,
        error
    };
}

// eslint-disable-next-line import/prefer-default-export
export function getAllowedValues() {
    return (dispatch) => {
        //return the api call results and you can alert the store to update
        //its state from here,
    };
}